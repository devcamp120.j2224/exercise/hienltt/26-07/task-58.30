package com.devcamp.s50.task5830.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5830.restapi.model.CCustomer;
import com.devcamp.s50.task5830.restapi.repository.ICustomerRepository;

@RestController
public class CCustomerController {
    @Autowired
    ICustomerRepository iCustomerRepository;

    @CrossOrigin
    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer(){
        try {
            List<CCustomer> lisCCustomers = new ArrayList<CCustomer>();
            iCustomerRepository.findAll().forEach(lisCCustomers::add);
            return new ResponseEntity<>(lisCCustomers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
}
