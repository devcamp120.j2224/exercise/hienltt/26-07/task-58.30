package com.devcamp.s50.task5830.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5830.restapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    
}
